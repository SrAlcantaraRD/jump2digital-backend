import { ISkin } from "./skin.model";

export interface ISkinService {
  getAllAvailable(): Promise<Array<ISkin>>;
  getById(id: number): Promise<ISkin>;
  getMySkins(): Promise<Array<ISkin>>;
  buySkin(id: ISkin): Promise<ISkin>;
  changeColor(id: ISkin): Promise<ISkin>;
  deleteSkin(id: number): Promise<ISkin>;
}

export default class SkinService implements ISkinService {
  async getAllAvailable(): Promise<ISkin[]> {
    throw new Error("Method not implemented.");
  }

  async getById(id: number): Promise<ISkin> {
    throw new Error("Method not implemented.");
  }

  async getMySkins(): Promise<ISkin[]> {
    throw new Error("Method not implemented.");
  }

  async buySkin(id: ISkin): Promise<ISkin> {
    throw new Error("Method not implemented.");
  }

  async changeColor(id: ISkin): Promise<ISkin> {
    throw new Error("Method not implemented.");
  }

  async deleteSkin(id: number): Promise<ISkin> {
    throw new Error("Method not implemented.");
  }
}
