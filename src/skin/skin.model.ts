import { t } from "elysia";

export interface ISkin {
  id: number;
  nombre: string;
  tipos: string;
  precio: number;
  color: string;
}

export const SkinParams = t.Object({ id: t.Numeric() });

export const SkinDTO = t.Object({
  id: t.Numeric(),
  nombre: t.String(),
  tipos: t.String(),
  precio: t.Numeric(),
  color: t.String(),
});
