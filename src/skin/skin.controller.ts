import Elysia from "elysia";
import { SkinDTO, SkinParams } from "./skin.model";
import SkinService from "./skin.service";

const SkinController = new Elysia({ prefix: "/skin" })
  .decorate("skinService", new SkinService())
  .get("/avaible", ({ skinService }) => skinService.getAllAvailable())
  .get(
    "/getskin/:id",
    ({ skinService, params }) => skinService.getById(params.id),
    {
      params: SkinParams,
    }
  )
  .get("/myskins", ({ skinService }) => skinService.getMySkins())
  .post("/buy", ({ skinService, body }) => skinService.buySkin(body), {
    body: SkinDTO,
  })
  .put("/color", ({ skinService, body }) => skinService.changeColor(body), {
    body: SkinDTO,
  })
  .delete(
    "/delete/:id",
    ({ skinService, params }) => skinService.deleteSkin(params.id),
    {
      params: SkinParams,
    }
  );

export default SkinController;
