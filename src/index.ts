import { swagger } from "@elysiajs/swagger";
import { Elysia } from "elysia";

import SkinController from "@skin/skin.controller";

const app = new Elysia()
  .group("/v1", (app) =>
    app
      .use(swagger())
      .get("/", () => "v1 | 🦊 Elysia API ")
      .use(SkinController)
  )
  .listen(3000);
